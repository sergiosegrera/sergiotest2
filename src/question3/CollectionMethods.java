package question3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class CollectionMethods {
	public static Collection<Planet> getInnerPlanets(Collection<Planet> planets) {
		Iterator<Planet> iterator = planets.iterator();
		
		Collection<Planet> firstPlanets = new ArrayList<Planet>();
		
		while (iterator.hasNext()) {
			Planet planet = iterator.next();
			if (planet.getOrder() < 4) {
				firstPlanets.add(planet);
			}
		}
		return firstPlanets;
	}
	
	public static void main(String[] args) {
		Collection<Planet> planets = new ArrayList<Planet>();
		
		planets.add(new Planet("Solar", "Earth", 3, 30));
		planets.add(new Planet("Solar", "Saturn", 6, 70));
		planets.add(new Planet("Solar", "Venus", 2, 25));
		
		System.out.println(getInnerPlanets(planets).toString());
	}

}
