package question3;

import java.util.Arrays;

// Test class for Planet class
public class PlanetMain {

	public static void main(String[] args) {
		Planet[] planets = new Planet[3];
		planets[0] = new Planet("B", "Earth", 1, 3);
		planets[1] = new Planet("A", "Earth", 1, 3);
		planets[2] = new Planet("C", "Earth", 1, 7);
		
		System.out.println(planets[0].hashCode());
		System.out.println(planets[1].hashCode());
		System.out.println(planets[2].hashCode());
		
		Arrays.sort(planets);
		
		System.out.println(Arrays.toString(planets));
	}

}
