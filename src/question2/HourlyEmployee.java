package question2;

public class HourlyEmployee implements Employee {
	private double hours;
	private double pay;
	
	HourlyEmployee(double hours, double pay) {
		this.hours = hours;
		this.pay = pay;
	}
	
	public double getHours() {
		return this.hours;
	}
	
	public double getHourlyPay() {
		return this.pay;
	}
	
	@Override
	public double getYearlyPay() {
		return this.hours * this.pay * 52;
	}

}
