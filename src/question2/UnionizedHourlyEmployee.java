package question2;

public class UnionizedHourlyEmployee extends HourlyEmployee {
	private double pensionContribution;
	
	UnionizedHourlyEmployee(double hours, double pay, double pensionContribution) {
		super(hours, pay);
		this.pensionContribution = pensionContribution;
	}

	@Override
	public double getYearlyPay() {
		return getHours() * getHourlyPay() * 52 + this.pensionContribution;
	}
}
