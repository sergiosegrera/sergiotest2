package question2;

public class PayrollManagement {
	public static double getTotalExpenses(Employee[] employees) {
		double total = 0;
		for (int i = 0; i < employees.length; i++) {
			total += employees[i].getYearlyPay();
			System.out.println(total);
		}
		
		return total;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Employee[] employees = new Employee[5];
		
		employees[0] = new SalariedEmployee(30000);
		employees[1] = new HourlyEmployee(40, 15.25);
		employees[2] = new HourlyEmployee(50, 20);
		employees[3] = new UnionizedHourlyEmployee(40, 15, 5000);
		employees[4] = new UnionizedHourlyEmployee(30, 30.50, 1000);
		
		System.out.println(getTotalExpenses(employees));
	}

}
