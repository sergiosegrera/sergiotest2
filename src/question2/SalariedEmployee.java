package question2;

public class SalariedEmployee implements Employee {
	private double yearly;
	
	SalariedEmployee(double yearlyPay) {
		this.yearly = yearlyPay;
	}
	
	@Override
	public double getYearlyPay() {
		return this.yearly;
	}
}
